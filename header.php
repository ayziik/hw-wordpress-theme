<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hikers Blog &mdash; Colorlib Website Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<?php wp_head(); ?>
</head>
<body>

<div class="site-wrap">

    <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>

    <header class="site-navbar pt-3" role="banner">
        <div class="container-fluid">
            <div class="row align-items-center">

                <div class="col-6 col-xl-6 logo">
                    <h1 class="mb-0"><a href="index.php" class="text-black h2 mb-0">Hikers</a></h1>
                </div>

                <div class="col-6 mr-auto py-3 text-right" style="position: relative; top: 3px;">
                    <div class="social-icons d-inline">
                        <a href="#"><span class="icon-facebook"></span></a>
                        <a href="#"><span class="icon-twitter"></span></a>
                        <a href="#"><span class="icon-instagram"></span></a>
                    </div>
                    <a href="#" class="site-menu-toggle js-menu-toggle text-black d-inline-block d-xl-none"><span
                                class="icon-menu h3"></span></a></div>
            </div>

            <div class="col-12 d-none d-xl-block border-top">
                <nav class="site-navigation text-center " role="navigation">
                    <?php
                        wp_nav_menu( [
                            'menu'            => 'Main',
                            'container'       => 'false',
                            'menu_class'      => 'site-menu js-clone-nav mx-auto d-none d-lg-block mb-0',
                            'echo'            => true,
                            'fallback_cb'     => 'wp_page_menu',
                            'items_wrap'      => '<ul class="site-menu js-clone-nav mx-auto d-none d-lg-block mb-0">%3$s</ul>',
                            'depth'           => 1,
                        ] );
                    ?>

<!--                    <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block mb-0">-->
<!--                        <li class="active"><a href="index.php">Homepage</a></li>-->
<!--                        <li><a href="category.php">Lifestyle</a></li>-->
<!--                        <li class="has-children">-->
<!--                            <a href="category.php">Inspiration</a>-->
<!--                            <ul class="dropdown">-->
<!--                                <li><a href="category.php">Architect</a></li>-->
<!--                                <li><a href="category.php">Minimal</a></li>-->
<!--                                <li><a href="category.php">Interior</a></li>-->
<!--                                <li><a href="category.php">Furniture</a></li>-->
<!--                            </ul>-->
<!--                        </li>-->
<!--                        <li><a href="category.php">Technology</a></li>-->
<!--                        <li><a href="category.php">Latest</a></li>-->
<!--                    </ul>-->
                </nav>
            </div>
        </div>
    </header>