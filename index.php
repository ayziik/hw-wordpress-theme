<?php get_header(); ?>

<div class="slide-one-item home-slider owl-carousel">

    <div class="site-cover site-cover-sm same-height overlay" style="background-image: url('<?php echo bloginfo('template_url'); ?>/assets/images/img_1.jpg');">
        <div class="container">
            <div class="row same-height align-items-center">
                <div class="col-md-12 col-lg-6">
                    <div class="post-entry">
                        <span class="post-category text-white bg-success mb-3">Nature</span>
                        <h2 class="mb-4"><a href="#">The 20 Biggest Fintech Companies In America 2019</a></h2>
                        <div class="post-meta align-items-center text-left">
                            <figure class="author-figure mb-0 mr-3 float-left"><img src="<?php echo bloginfo('template_url'); ?>/assets/images/person_1.jpg"
                                                                                    alt="Image" class="img-fluid">
                            </figure>
                            <span class="d-inline-block mt-1">By Carrol Atkinson</span>
                            <span>&nbsp;-&nbsp; February 10, 2019</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-cover site-cover-sm same-height overlay" style="background-image: url('<?php echo bloginfo('template_url'); ?>/assets/images/img_2.jpg');">
        <div class="container">
            <div class="row same-height align-items-center">
                <div class="col-md-12 col-lg-6">
                    <div class="post-entry">
                        <span class="post-category text-white bg-success mb-3">Nature</span>
                        <h2 class="mb-4"><a href="#">The 20 Biggest Fintech Companies In America 2019</a></h2>
                        <div class="post-meta align-items-center text-left">
                            <figure class="author-figure mb-0 mr-3 float-left"><img src="<?php echo bloginfo('template_url'); ?>/assets/images/person_1.jpg"
                                                                                    alt="Image" class="img-fluid">
                            </figure>
                            <span class="d-inline-block mt-1">By Carrol Atkinson</span>
                            <span>&nbsp;-&nbsp; February 10, 2019</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="site-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-12 section-heading"><h2>Popular Posts</h2></div>
        </div>
        <div class="row">
            <div class="col-lg-6 mb-5 mb-lg-0">
                <div class="entry2">
                    <a href="single.php"><img src="<?php echo bloginfo('template_url'); ?>/assets/images/img_1.jpg" alt="Image" class="img-fluid rounded"></a>
                    <span class="post-category text-white bg-success mb-3">Nature</span>
                    <h2><a href="single.php">The 20 Biggest Fintech Companies In America 2019</a></h2>
                    <div class="post-meta align-items-center text-left clearfix">
                        <figure class="author-figure mb-0 mr-3 float-left"><img src="<?php echo bloginfo('template_url'); ?>/assets/images/person_1.jpg"
                                                                                alt="Image" class="img-fluid"></figure>
                        <span class="d-inline-block mt-1">By <a href="#">Carrol Atkinson</a></span>
                        <span>&nbsp;-&nbsp; February 10, 2019</span>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium sed
                        optio, explicabo ad deleniti impedit facilis fugit recusandae! Illo, aliquid, dicta beatae quia
                        porro id est.</p>
                </div>
            </div>
            <div class="col-lg-6 pl-lg-4">
                <div class="entry3 d-block d-sm-flex">
                    <figure class="figure order-2"><a href="single.php"><img src="<?php echo bloginfo('template_url'); ?>/assets/images/img_2.jpg" alt="Image"
                                                                              class="img-fluid rounded"></a></figure>
                    <div class="text mr-4 order-1">
                        <span class="post-category text-white bg-success mb-3">Nature</span>
                        <h2><a href="single.php">The 20 Biggest Fintech Companies In America 2019</a></h2>
                        <span class="post-meta mb-3 d-block">May 12, 2019</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium
                            sed optio.</p>
                    </div>
                </div>

                <div class="entry3 d-block d-sm-flex">
                    <figure class="figure order-2"><a href="single.php"><img src="<?php echo bloginfo('template_url'); ?>/assets/images/img_3.jpg" alt="Image"
                                                                              class="img-fluid rounded"></a></figure>
                    <div class="text mr-4 order-1">
                        <span class="post-category text-white bg-success mb-3">Nature</span>
                        <h2><a href="single.php">The 20 Biggest Fintech Companies In America 2019</a></h2>
                        <span class="post-meta mb-3 d-block">May 12, 2019</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium
                            sed optio.</p>
                    </div>
                </div>

                <div class="entry3 d-block d-sm-flex">
                    <figure class="figure order-2"><a href="single.php"><img src="<?php echo bloginfo('template_url'); ?>/assets/images/img_4.jpg" alt="Image"
                                                                              class="img-fluid rounded"></a></figure>
                    <div class="text mr-4 order-1">
                        <span class="post-category text-white bg-success mb-3">Nature</span>
                        <h2><a href="single.php">The 20 Biggest Fintech Companies In America 2019</a></h2>
                        <span class="post-meta mb-3 d-block">May 12, 2019</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium
                            sed optio.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>