<?php

add_action( 'wp_enqueue_scripts', 'hwAddScripts' );

function hwAddScripts() {
	wp_enqueue_style( 'hwMainStyle', get_stylesheet_uri() );
	wp_enqueue_style( 'icomoonStyle', get_template_directory_uri() . '/assets/fonts/icomoon/style.css' );
	wp_enqueue_style( 'bootstrapMin', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
	wp_enqueue_style( 'magnificPopup', get_template_directory_uri() . '/assets/css/magnific-popup.css' );
	wp_enqueue_style( 'jqueryUI', get_template_directory_uri() . '/assets/css/jquery-ui.css' );
	wp_enqueue_style( 'owlCarouselMin', get_template_directory_uri() . '/assets/css/owl.carousel.min.css' );
	wp_enqueue_style( 'owlThemeDefaultMin', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css' );
	wp_enqueue_style( 'bootstrapDatepicker', get_template_directory_uri() . '/assets/css/bootstrap-datepicker.css' );
	wp_enqueue_style( 'flaticonStyle', get_template_directory_uri() . '/assets/fonts/flaticon/font/flaticon.css' );
	wp_enqueue_style( 'aosStyle', get_template_directory_uri() . '/assets/css/aos.css' );
	wp_enqueue_style( 'googleFonts', 'https://fonts.googleapis.com/css?family=Muli:300,400,700|Playfair+Display:400,700,900' );

	wp_enqueue_script( 'jqueryMin', get_template_directory_uri() . '/assets/js/jquery-3.3.1.min.js',
		array(), null, true );
	wp_enqueue_script( 'jqueryMigrate', get_template_directory_uri() . '/js/jquery-migrate-3.0.1.min.js',
		array(), null, true );
	wp_enqueue_script( 'jqueryUiJs', get_template_directory_uri() . '/assets/js/jquery-ui.js',
		array(), null, true );
	wp_enqueue_script( 'popperMin', get_template_directory_uri() . '/assets/js/popper.min.js',
		array(), null, true );
	wp_enqueue_script( 'bootstrapMinJs', get_template_directory_uri() . '/assets/js/bootstrap.min.js',
		array(), null, true );
	wp_enqueue_script( 'owlCarouselMinJs', get_template_directory_uri() . '/assets/js/owl.carousel.min.js',
		array(), null, true );
	wp_enqueue_script( 'jqueryStellarMin', get_template_directory_uri() . '/assets/js/jquery.stellar.min.js',
		array(), null, true );
	wp_enqueue_script( 'jqueryCountdownMin', get_template_directory_uri() . '/assets/js/jquery.countdown.min.js',
		array(), null, true );
	wp_enqueue_script( 'jqueryMagnificPopup', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js',
		array(), null, true );
	wp_enqueue_script( 'bootstrapDatepickerMinJs', get_template_directory_uri() . '/assets/js/bootstrap-datepicker.min.js',
		array(), null, true );
	wp_enqueue_script( 'aosJs', get_template_directory_uri() . '/assets/js/aos.js',
		array(), null, true );
	wp_enqueue_script( 'mainJs', get_template_directory_uri() . '/assets/js/main.js',
		array( 'jquery' ), null, true );
}

add_theme_support( 'menus' );

add_filter( 'nav_menu_css_class', 'change_menu_item_args', 10, 4 );

function change_menu_item_args( $classes, $item, $args, $depth ) {
	if ( $item->current ) {
		array_push( $classes, 'active' );
	}

	return $classes;
}